import am.egs.mvc.model.User;
import am.egs.mvc.repository.UserRepository;
import am.egs.mvc.repository.implementations.UserRepository_Impl;

public class main {

    public static void main(String[] args) {

        User user = new User();
        UserRepository_Impl userRepository = new UserRepository_Impl();
        user.setName("Artak");
        user.setEmail("ahha@mail.ru");
        user.setPassword("artakpass");
        userRepository.add(user);
    }

}
