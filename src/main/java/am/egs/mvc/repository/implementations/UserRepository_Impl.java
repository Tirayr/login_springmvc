package am.egs.mvc.repository.implementations;


import am.egs.mvc.model.User;
import am.egs.mvc.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;



@Repository
public class UserRepository_Impl implements UserRepository {

    private static Logger logger = Logger.getLogger(UserRepository_Impl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void add(User user) {
        String sql_insert = "insert into user(name,email,password) values (?,?,?)";
        jdbcTemplate.update(sql_insert, user.getName(), user.getEmail(), user.getPassword());
        logger.info("addUser()::" + user.toString());
    }

    public User getByEmail(String email) {
        String sql_select = "select * from user where email = ?";
        User user = (User) jdbcTemplate.queryForObject(sql_select, new Object[]{email}, new BeanPropertyRowMapper(User.class));
        return user;
    }

    public int countofDuplicate(String email) {
        String sql_count = "select count(*) from user where email = ?";
        int count = jdbcTemplate.queryForObject(sql_count,new Object[]{email}, Integer.class);
        return count;
    }

}