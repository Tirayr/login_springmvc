package am.egs.mvc.repository;

import am.egs.mvc.model.User;

import java.sql.SQLException;

public interface UserRepository {
    void add(User user);

    User getByEmail(String email) throws SQLException;

    int countofDuplicate(String email);
}
