package am.egs.mvc.util;

public class Util {

    public static String validate(String ... params){

        for (String param : params){
            if (param == null || param.equals("")){
                return  "All params are required";
            }
        }
        return null;
    }

}
