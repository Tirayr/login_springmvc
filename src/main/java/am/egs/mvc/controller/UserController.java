package am.egs.mvc.controller;


import am.egs.mvc.model.User;
import am.egs.mvc.service.UserService;
import am.egs.mvc.util.exceptions.DuplicateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;


@Controller
public class UserController {



    @Autowired
    private UserService userService;

    @Autowired
    private User user;



    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView register(@RequestParam String name,
                                 @RequestParam String email,
                                 @RequestParam String password,
                                 HttpSession session) {
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        try {
            userService.add(user);
            session.setAttribute("user", user);
            return new ModelAndView("home");
        } catch (DuplicateException e) {
            return new ModelAndView("register","message",e.getMessage());
        }



    }

    @RequestMapping(value = "/login", method =RequestMethod.POST)
    public ModelAndView login(@RequestParam String email,
                              @RequestParam String password,
                              HttpSession session) throws SQLException {

        user = userService.getUser(email);
        if (!user.getPassword().equals(password)){
            return new ModelAndView("index","message","wrong email or password");
        }
        return new ModelAndView("home");
    }




}
