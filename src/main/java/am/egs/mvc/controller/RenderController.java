package am.egs.mvc.controller;

//import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;


@Controller
public class RenderController {

   // private final Logger logger = Logger.getLogger(RenderController.class);

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String start(){
       /* if (logger.isInfoEnabled()){
            logger.info("method: doGet(), page:/index.jsp");
        }*/
        return "index";
    }

    @RequestMapping(value = "/goToRegister",method = RequestMethod.GET)
    public String goToRegister(){
        return "register";
    }

    @RequestMapping(value = "/logout",method = RequestMethod.GET)
    public String logout(HttpSession session){
        session.invalidate();
        return "index";
    }

}
