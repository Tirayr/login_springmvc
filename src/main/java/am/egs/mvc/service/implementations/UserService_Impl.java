package am.egs.mvc.service.implementations;


import am.egs.mvc.model.User;
import am.egs.mvc.repository.UserRepository;
import am.egs.mvc.service.UserService;
import am.egs.mvc.util.exceptions.DuplicateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;

@Service
public class UserService_Impl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Transactional
    public void add(User user) throws DuplicateException {
        int countOfDuplicate = userRepository.countofDuplicate(user.getEmail());
        if (countOfDuplicate != 0){
            throw new DuplicateException("There is user with this email");
        }
        userRepository.add(user);
    }

    public User getUser(String email) throws SQLException {
        return userRepository.getByEmail(email);
    }
}
