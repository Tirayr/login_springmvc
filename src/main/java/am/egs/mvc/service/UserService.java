package am.egs.mvc.service;

import am.egs.mvc.model.User;
import am.egs.mvc.util.exceptions.DuplicateException;

import java.sql.SQLException;

public interface UserService {
    void add(User user) throws DuplicateException;

    User getUser(String email) throws SQLException;
}
